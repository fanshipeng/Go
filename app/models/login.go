package models

import "github.com/astaxie/beego/orm"

type Login struct {
	Id int
	Name string
	Prd string
}


//先执行
func init()  {
	orm.RegisterModel(new(Login))
}

func (o *Login) TableName() string {
	return "login"
}