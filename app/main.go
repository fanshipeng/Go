package main

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	_"github.com/go-sql-driver/mysql"
	_"xiangmu/app/routers"
)

func main() {
	beego.Run()
}

//限执行
func init()  {
	orm.Debug = true
	//开启日志
	logs.SetLogger(logs.AdapterFile, `{"filename":"app.log", "level":7}`)
	//开启调错模式
	orm.Debug = true

	mysqlhost:=beego.AppConfig.String("dbhost")
	mysqlport:=beego.AppConfig.String("dbport")
	mysqluser:=beego.AppConfig.String("dbuser")
	mysqlprd:=beego.AppConfig.String("dbprd")
	mysqlname:=beego.AppConfig.String("dbname")

	con:=mysqluser+":"+mysqlprd+"@tcp("+mysqlhost+":"+mysqlport+")/"+mysqlname+"?charset=utf8&parseTime=true&loc=Local"
	logs.Info(con)

	orm.RegisterDataBase("default","mysql",con)
	//orm.RunSyncdb("default",false,true)
}

