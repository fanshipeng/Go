package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	"time"
	"xiangmu/app/models"
)
type Login struct {
	Id int
	Name string
	Prd string
}

type User struct {
	Name string `form:"name"`
	Tel int`form:"tel"`
	Email string	`form:"email"`
	Sex string		`form:"sex"`
	Borth string	`form:"borth"`
	Hobby []string `form:"hobby"`
}

type MainController struct {
	beego.Controller
}

func (c *MainController) WW() {
	fmt.Println(models.Login{})
}

func (c *MainController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "index.tpl"
}
//入口文件
func (c *MainController)Rukou()  {
	c.TplName = "fsp.html"
}

//用户首页
func (c *MainController)First()  {
	c.Data["rukou"] = "Welcome to .."
	c.TplName = "first.html"
}

//用户登录
func (c *MainController)Login()  {
	c.TplName = "login.html"
}
//登录验证
func (c *MainController)DoLogin()  {
	//接收表单传来的数据
	name:=c.GetString("name")
	prd:=c.GetString("prd")

	dblogin:=new(models.Login)
	//初始化orm
	o:=orm.NewOrm()
	o.QueryTable("login").Filter("name",name).Filter("prd",prd).One(dblogin)
	//判断传值的类型
	if c.Ctx.Input.IsGet(){
		if name==""||prd==""{
			c.Ctx.WriteString("<script>alert('请输入完整的信息');location.href='login';</script>")

		}else{
			if name==dblogin.Name&&prd==dblogin.Prd{
				//cookie存
				//c.Ctx.SetCookie("cook",name)
				c.SetSession("cook",name)
				//跳转
				c.Ctx.WriteString("<script>alert('登录成功');location.href='first';</script>")
			}else{
				c.Ctx.WriteString("<script>alert('账号或密码错误');location.href='login';</script>")
			}
		}
	}else{
		c.Ctx.WriteString("请换一种输入的方式")
	}
}

//用户表单
func (c *MainController)Form()  {
	//a:=c.Ctx.GetCookie("cook")
	c.GetSession("cook")
	if c.GetSession("cook")!=nil{
		c.TplName = "form.html"
	}else{
		c.TplName = "login.html"
	}
}

//表单操作
func (c *MainController)DoForm()  {
	u:=User{}
	if err:=c.ParseForm(&u);err!=nil{
		beego.Info()
	}else{
		logs.Debug(u)
		valid:=validation.Validation{}
		valid.Required(u.Name,"姓名必填")
		valid.Required(u.Tel,"手机号必填")
		if valid.HasErrors() {
				c.Data["json"] = valid.Errors
		}else{
			c.Data["json"] = u
		}
		c.ServeJSON()
	}
}
//个人信息
func (c *MainController)Geren()  {
	//a:=c.Ctx.GetCookie("cook")
	c.GetSession("cook")
	if c.GetSession("cook")!=nil{
		if B.IsExist("list"){
			var list interface{}
			list =B.Get("list")
			c.Data["list"] = list
			logs.Debug("Cache读取")
			c.TplName = "geren.html"
		}else{
			//模型层
			var login []*models.Login
			//初始化orm
			o:=orm.NewOrm()
			//查询数据库的所有数据
			o.QueryTable("login").OrderBy("id").All(&login)
			c.Data["list"] =login
			B.Put("list",login,10*time.Second)
			c.TplName = "geren.html"
			logs.Debug("数据库读取")
		}
	}else{
		c.TplName = "login.html"
	}
}
//我的订单
func (c *MainController)Ding()  {
	////a:=c.Ctx.GetCookie("cook")
	//a:=c.GetSession("cook")
	if c.GetSession("cook")!=nil{
		c.Data["dingdan"] = "我的订单"
		c.TplName = "dingdan.html"
	}else{
		c.TplName = "login.html"
	}
}

//收藏
func (c *MainController)ShouCang()  {
	//a:=c.Ctx.GetCookie("cook")
	if c.GetSession("cook")!=nil{
		c.Data["shoucang"] = "我的收藏"
		c.TplName = "shoucang.html"
	}else{
		c.TplName = "login.html"
	}
}

//退出登录
func (c *MainController)Tui() {

	//c.Ctx.GetCookie("cook")

	//c.Ctx.SetCookie("cook"," ")
	c.GetSession("cook")
	c.DelSession("cook")
	c.Ctx.WriteString("<script>alert('退出成功');location.href='rukou';</script>")
}

//注册
func (c *MainController)ZhuCe()  {
	c.TplName = "zhuce.html"
}

//执行注册
func (c *MainController)Dozhuce()  {
	//模型层
	data:=models.Login{}

	//获取输入的值
	name:=c.GetString("name")
	prd:=c.GetString("prd")
	data.Name = name
	data.Prd = prd

	if name!="" &&prd!=""{
		//初始化orm
		o:=orm.NewOrm()
		_,err:=o.Insert(&data)
		if err==nil{
			c.Ctx.WriteString("<script>alert('注册成功');location.href='rukou'</script>")
		}
	}else{
		c.Ctx.WriteString("失败")
	}
}
















//连接
var B cache.Cache
func init()  {
	bm, err := cache.NewCache("memory", `{"interval":60}`)
	if err!=nil{
		logs.Debug(err)
	}

	B = bm
}












//修改页面
func (c *MainController)Up()  {
	//接收修改的id
	id,_:=c.GetInt("id")
	//模型层
	login:=models.Login{}
	//初始化orm
	o:=orm.NewOrm()
	//查询数据库的所有数据
	o.QueryTable("login").Filter("id",id).One(&login)
	c.Data["fsp"] =login
	c.TplName = "up.html"
}

//执行修改
func (c *MainController)DoUp()  {

	//接收修改的id
	//修改的内容
	name:=c.GetString("name")
	prd:=c.GetString("prd")
	id,_:=c.GetInt("id")

	//u:=Login{}
	//	//u.Name = name
	//	//u.Prd = prd
	//模型层
	login:=models.Login{Name:name,Prd:prd,Id:id}
	//初始化orm
	o:=orm.NewOrm()

	_,err1:=o.Update(&login)
	if err1!=nil{
		c.Ctx.WriteString("<script>alert('更新失败');location.href='geren';</script>")
	}else{
		c.Ctx.WriteString("<script>alert('更新成功');location.href='geren';</script>")
	}
}

//删除
func (c *MainController)Del()  {
	//获取删除的id
	id,_:=c.GetInt("id")

	login:=models.Login{Id:id}
	//初始化orm
	o:=orm.NewOrm()
	_,err:=o.Delete(&login)
	if err!=nil{
		c.Ctx.WriteString("<script>alert('删除失败');location.href='geren';</script>")
	}else{
		c.Ctx.WriteString("<script>alert('删除成功');location.href='geren';</script>")
	}
}
