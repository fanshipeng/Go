package routers

import (
	"xiangmu/app/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})

    //自动匹配
    beego.AutoRouter(&controllers.MainController{})
    //入口
    beego.Router("/rukou",&controllers.MainController{},"get:Rukou")
    //用户页
    beego.Router("/first",&controllers.MainController{},"get:First")
    //用户登录
    beego.Router("/login",&controllers.MainController{},"get:Login")
    //登录验证
    beego.Router("/dologin",&controllers.MainController{},"get:DoLogin")
    //表单
    beego.Router("/form",&controllers.MainController{},"get:Form")
    //表单操作
    beego.Router("doform",&controllers.MainController{},"get:DoForm")
    //个人信息
    beego.Router("/geren",&controllers.MainController{},"get:Geren")
    //我的订单
	beego.Router("/dingdan",&controllers.MainController{},"get:Ding")
    //我的收藏
    beego.Router("shoucang",&controllers.MainController{},"get:ShouCang")
    //退出登录
    beego.Router("tui",&controllers.MainController{},"get:Tui")
    //用户注册
    beego.Router("zhuce",&controllers.MainController{},"get:ZhuCe")
    //执行用户注册
    beego.Router("dozhuce",&controllers.MainController{},"get:Dozhuce")


    //修改页面
    beego.Router("up",&controllers.MainController{},"get:Up")
    //执行修改页面
    beego.Router("doup",&controllers.MainController{},"get:DoUp")
    //执行删除
    beego.Router("del",&controllers.MainController{},"get:Del")
}
